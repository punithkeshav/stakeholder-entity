/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_View_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Add Initiatives - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR21_Add_View_Initiatives_Alternate_Scenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR21_Add_View_Initiatives_Alternate_Scenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderEntity())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToStakeholderEntity(){
        
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to wait for 'Stakeholders Entity' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to click on 'Stakeholders Entity' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Entity' tab.");
        
        pause(10000);
        
        //Entity Name Search
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.SE_EntityNameSearchBox(), testData.getData("SE_EntityNameSearch"))){
            error = "Failed to enter '"+testData.getData("SE_EntityNameSearch")+"' into 'SE_EntityNameSearch' input.";
            return false;
        }
        
        pause(2000);
        
        //Search Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to wait for Search Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to click on Search Button";
            return false;
        }
        
        SeleniumDriverInstance.pause(15000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Search Button");
        
        //Stakeholder Selection
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.SE_RecordSelection())){
            error = "Failed to wait for Stakeholder Selection";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.SE_RecordSelection())){
            error = "Failed to click on Stakeholder Selection";
            return false;
        }
        
        pause(15000);
        
        //Right Arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to wait for 'Left' Arrow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }
        pause(500);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.TabScrollRight())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked 'Left' Arrow.");

        //Set the page as a parent page
        String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        
        //Initiatives tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.initiativesTab())){
            error = "Failed to wait for Initiatives tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.initiativesTab())){
            error = "Failed to click on Initiatives tab";
            return false;
        }
        
        pause(1000);
        
        //Add an Initiative button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.addInitiativeButton())){
            error = "Failed to wait for Add an Initiative button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.addInitiativeButton())){
            error = "Failed to click on Add an Initiative button";
            return false;
        }
        
        //switch to the new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch on the 'Engagement Plan' window.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched to the 'Engagement Plan' window..");
        
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //process flow
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.initiativesProcessFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.initiativesProcessFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_View_PageObjects.processFlowStatus("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Business Unit Dropdown
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }
    
        pause(1000);
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.selectAllDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown select all Tick";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.selectAllDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown select all Tick";
            return false;
        }
        
        //Project title
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.projectTitle()))
        {
            error = "Failed to locate Project title field";
            return false;
        }   
      
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.projectTitle(),testData.getData("Project title")))
        {
            error = "Failed to enter text in Project title field";
            return false;
        }
       
        //Type of Initiative dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.typeOfInitiativeDropdown()))
        {
            error = "Failed to locate Type of Initiative Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.typeOfInitiativeDropdown()))
        {
            error = "Failed to click on Type of Initiative Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.typeOfInitiativeDropdownValue(testData.getData("Type of Initiative"))))
        {
            error = "Failed to locate Type of Initiative Dropdown value chevron";
            return false;
        }
        
         if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.typeOfInitiativeDropdownValue(testData.getData("Type of Initiative"))))
        {
            error = "Failed to click Type of Initiative Dropdown value chevron";
            return false;
        }
         
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.typeOfInitiativeDropdownChildValue(testData.getData("Type of Initiative value"))))
        {
            error = "Failed to locate Type of Initiative Dropdown value";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.typeOfInitiativeDropdownChildValue(testData.getData("Type of Initiative value"))))
        {
            error = "Failed to click Type of Initiative Dropdown value";
            return false;
        } 
         
        //Commencement Date
     	String commencementdate=SeleniumDriverInstance.getADate("", 0);
         
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.commencementDateXpath(), commencementdate))
        {
            error = "Failed to enter Commencement date";
            return false;
        }

        //Delivery Date
     	String deliverydate=SeleniumDriverInstance.getADate("", 2);
         
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.deliveryDateXpath(), deliverydate))
        {
            error = "Failed to enter Delivery date";
            return false;
        }

        //Approved budget
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.approvedBudget()))
        {
            error = "Failed to locate Approved budget field";
            return false;
        }   
      
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.approvedBudget(),testData.getData("Approved budget")))
        {
            error = "Failed to enter text in Approved budget field";
            return false;
        }
        
         //Location Dropdown
         if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.initaivesLocationDropdown()))
        {
            error = "Failed to locate Location Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.initaivesLocationDropdown()))
        {
            error = "Failed to click on Location Dropdown";
            return false;
        }
    
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.expandChevron("2")))
        {
            error = "Failed to locate Business Unit expand chevron";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.expandChevron("2")))
        {
            error = "Failed to click Business Unit expand chevron";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.expandChevron("3")))
        {
            error = "Failed to locate Business Unit expand chevron";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.expandChevron("3")))
        {
            error = "Failed to click Business Unit expand chevron";
            return false;
        }
         
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.singleSelect(testData.getData("Location"))))
        {
            error = "Failed to click Business Unit Dropdown value";
            return false;
        }
        
        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.responsiblePersonDropdown()))
        {
            error = "Failed to locate Responsible person";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.responsiblePersonDropdown()))
        {
            error = "Failed to click on Responsible person";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.responsiblePersonDropdownValue(testData.getData("Responsible person"))))
        {
            error = "Failed to locate Responsible person value";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.responsiblePersonDropdownValue(testData.getData("Responsible person"))))
        {
            error = "Failed to click on Responsible person Dropdown value";
            return false;
        }

        //Sector Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.sectorDropdown()))
        {
            error = "Failed to locate Sector Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.sectorDropdown()))
        {
            error = "Failed to click on Sector Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.singleSelect(testData.getData("Sector"))))
        {
            error = "Failed to locate Sector value";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.singleSelect(testData.getData("Sector"))))
        {
            error = "Failed to click on Sector Dropdown value";
            return false;
        } 
        
        //Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.description()))
        {
            error = "Failed to locate Description field";
            return false;
        }   
      
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.description(),testData.getData("Description")))
        {
            error = "Failed to enter text in Description field";
            return false;
        }
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        } 
        
        pause(15000);
        //Get the Record No
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_View_PageObjects.socialInitiativesRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
       
        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Entity_View_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }

        pause(2000);
        
        narrator.stepPassedWithScreenShot("Successfully added Social Initiatives record");
        
        return true;
    }

}

