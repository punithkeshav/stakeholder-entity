/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.previousDate;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsometricsPOCPageObjects;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Contractor Fleet Management - Main Scenario",
        createNewBrowserInstance = false
)

public class FR11_Contractor_Fleet_Management_Main_Scenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR11_Contractor_Fleet_Management_Main_Scenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 90);
        endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
 
    }

    public TestResult executeTest()
    {
        if (!navigateToFleetManagement()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterFleetManagementDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
       
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToFleetManagement(){    
        //Navigate to Fleet Management tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Fleetmanagementtab())){
            error = "Failed to wait for Fleet Management tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_Fleetmanagementtab())){
            error = "Failed to click on Fleet Management tab";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        narrator.stepPassedWithScreenShot("Successfully navigated to Fleet Management tab");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Fleetmanagementadd())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_Fleetmanagementadd())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Fleet Management Add' button.");

        return true;
    }
    
    public boolean enterFleetManagementDetails(){
        
        pause(5000);

        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.FleetmanagementPF())){
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.FleetmanagementPF())){
            error = "Failed to click on Process Flow button.";
            return false;
        }
        
        //Process Flow Add phase
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatusChild("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Vehicle type / make
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Vehicletypemake())){
            error = "Failed to wait for Vehicle type / make field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Vehicletypemake(), getData("Vehicle type / make"))){
            error = "Failed to click on 'Vehicle type / make' field.";
            return false; 
        }
        
        //Reg No.
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RegNo())){
            error = "Failed to wait for Vehicle type / make field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.RegNo(), getData("Reg No."))){
            error = "Failed to click on 'Vehicle type / make' field.";
            return false; 
        }
        
        //License expiry date
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Licenseexpirydate())) {
            error = "Failed to wait for 'License expiry date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Licenseexpirydate(), endDate)) {
            error = "Failed to enter '" + endDate + "' into License expiry date.";
            return false;
        }
        
        //Save Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.FleetmanagementSave())){
            error = "Failed to wait for Save Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.FleetmanagementSave())){
            error = "Failed to click on Save Button.";
            return false;
        }
        
        pause(15000);
        
        //Process Flow Add phase
        narrator.stepPassedWithScreenShot("Processflow in Edit phase");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatusChild("Edit phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Edit phase";
        return false;
        }
        
        return true;
        
    }
}
