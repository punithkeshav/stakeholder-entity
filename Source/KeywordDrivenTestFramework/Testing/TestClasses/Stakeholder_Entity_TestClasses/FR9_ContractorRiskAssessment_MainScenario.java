/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.previousDate;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsometricsPOCPageObjects;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Contractor Risk Assessment - Main Scenario",
        createNewBrowserInstance = false
)

public class FR9_ContractorRiskAssessment_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR9_ContractorRiskAssessment_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
 
    }

    public TestResult executeTest()
    {
        if (!navigateToRiskAssessment()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterRiskAssessmentDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
       
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToRiskAssessment(){    
        //Navigate to Risk Assessment tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_riskassessmentTab())){
            error = "Failed to wait for Risk Assessment tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_riskassessmentTab())){
            error = "Failed to click on Risk Assessment tab";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        narrator.stepPassedWithScreenShot("Successfully navigated to Risk Assessment tab");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_riskassessmentadd())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_riskassessmentadd())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Risk Assessment Add' button.");

        return true;
    }
    
    public boolean enterRiskAssessmentDetails(){
        
        pause(5000);
        
        String RiskassessmentCapturerLogger;
        RiskassessmentCapturerLogger = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.RiskassessmentCapturerLogger());
        
        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentPF())){
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentPF())){
            error = "Failed to click on Process Flow button.";
            return false;
        }
        
        //Process Flow Add phase
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatusChild("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Risk ranking date
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentRiskrankingdate())) {
            error = "Failed to wait for Risk ranking date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.RiskassessmentRiskrankingdate(), startDate)) {
            error = "Failed to enter Risk ranking date";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Risk ranking date");
        
        //Description of scope
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentDescriptionscope())){
            error = "Failed to wait for Description of scope field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.RiskassessmentDescriptionscope(), getData("Description of scope"))){
            error = "Failed to click on Description of scope field.";
            return false; 
        }
        
        //Related documents
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentRelatedDocs()))
        {
            error = "Failed to locate Related documents Tab";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentRelatedDocs()))
        {
            error = "Failed to click on Related documents Tab";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentRelatedDocsLinkDoc()))
        {
            error = "Failed to locate Medical Surveillance Supporting Documents Link Document";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentRelatedDocsLinkDoc()))
        {
            error = "Failed to click on Medical Surveillance Supporting Documents Link Document";
            return false;
        }
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        pause(1000);
        
        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentLinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.RiskassessmentLinkURL(), getData("HTTPURL") )){
            error = "Failed to enter URL value field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link");
        
        //Upload Link Title
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentURLTitle())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentURLTitle())){
            error = "Failed to enter Url Title field.";
            return false;
        }
            
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.RiskassessmentURLTitle(), getData("URLTitle"))){
            error = "Failed to enter Url Title field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("URL Title");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmenturlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmenturlAddButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded document using Link");
        
        //Switch to iframe
        if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";

        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Risk ranking tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentRiskRankingTab())){
            error = "Failed to wait for Risk ranking tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentRiskRankingTab())){
            error = "Failed to click on Risk ranking tab.";
            return false;
        }
        
        //1. Blasting activities including explosives
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment1BlastingDD())){
            error = "Failed to wait for Blasting activities including explosives dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment1BlastingDD())){
            error = "Failed to click on Blasting activities including explosives dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment1BlastingYes())){
            error = "Failed to wait for Blasting activities including explosives - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment1BlastingYes())){
            error = "Failed to click on Blasting activities including explosives - Yes";
            return false;
        }
        
        //2. Community impact
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment2CommunityDD())){
            error = "Failed to wait for 2. Community impact dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment2CommunityDD())){
            error = "Failed to click on 2. Community impact dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment2CommunityYes())){
            error = "Failed to wait for 2. Community impact - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment2CommunityYes())){
            error = "Failed to click on 2. Community impact - Yes";
            return false;
        }
        
        //3. Working in confined spaces
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment3WorkingDD())){
            error = "Failed to wait for 3. Working in confined spaces dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment3WorkingDD())){
            error = "Failed to click on 3. Working in confined spaces dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment3WorkingYes())){
            error = "Failed to wait for 3. Working in confined spaces - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment3WorkingYes())){
            error = "Failed to click on 3. Working in confined spaces - Yes";
            return false;
        }
        
        //4. Discharging of contaminated water
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment4DischargingDD())){
            error = "Failed to wait for 4. Discharging of contaminated water dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment4DischargingDD())){
            error = "Failed to click on 4. Discharging of contaminated water dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment4DischargingYes())){
            error = "Failed to wait for 4. Discharging of contaminated water - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment4DischargingYes())){
            error = "Failed to click on 4. Discharging of contaminated water - Yes";
            return false;
        }
        
        //5. Isolation and lock out
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment5IsolationDD())){
            error = "Failed to wait for 5. Isolation and lock out dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment5IsolationDD())){
            error = "Failed to click on 5. Isolation and lock out dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment5IsolationYes())){
            error = "Failed to wait for 5. Isolation and lock out - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment5IsolationYes())){
            error = "Failed to click on 5. Isolation and lock out - Yes";
            return false;
        }
        
        //6. Environmental impact
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment6EnvironmentalDD())){
            error = "Failed to wait for 6. Environmental impact dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment6EnvironmentalDD())){
            error = "Failed to click on 6. Environmental impact dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment6EnvironmentalYes())){
            error = "Failed to wait for 6. Environmental impact - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment6EnvironmentalYes())){
            error = "Failed to click on 6. Environmental impact - Yes";
            return false;
        }
        
        //7. Excavation work (Deeper than 1m)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment7ExcavationDD())){
            error = "Failed to wait for 7. Excavation work (Deeper than 1m) dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment7ExcavationDD())){
            error = "Failed to click on 7. Excavation work (Deeper than 1m) dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment7ExcavationYes())){
            error = "Failed to wait for 7. Excavation work (Deeper than 1m) - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment7ExcavationYes())){
            error = "Failed to click on 7. Excavation work (Deeper than 1m) - Yes";
            return false;
        }
        
        pause(1000);
        
        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_PageObjects.Riskassessment8ExplosionsLabel())){
            error = "Failed to click on 8. Explosions (electricity, gas, granulation, electrode, equipment) dropdown";
            return false;
        }
        
        pause(1000);
        
        //8. Explosions (electricity, gas, granulation, electrode, equipment)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment8ExplosionsDD())){
            error = "Failed to wait for 8. Explosions (electricity, gas, granulation, electrode, equipment) dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment8ExplosionsDD())){
            error = "Failed to click on 8. Explosions (electricity, gas, granulation, electrode, equipment) dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment8ExplosionsYes())){
            error = "Failed to wait for 8. Explosions (electricity, gas, granulation, electrode, equipment) - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment8ExplosionsYes())){
            error = "Failed to click on 8. Explosions (electricity, gas, granulation, electrode, equipment) - Yes";
            return false;
        }
        
        //9. Exposure to airborne pollutants
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment9ExposureDD())){
            error = "Failed to wait for 9. Exposure to airborne pollutants dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment9ExposureDD())){
            error = "Failed to click on 9. Exposure to airborne pollutants dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment9ExposureYes())){
            error = "Failed to wait for 9. Exposure to airborne pollutants - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment9ExposureYes())){
            error = "Failed to click on 9. Exposure to airborne pollutants - Yes";
            return false;
        }
        
        //10. Exposure to electricity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment10ExposureDD())){
            error = "Failed to wait for 10. Exposure to electricity dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment10ExposureDD())){
            error = "Failed to click on 10. Exposure to electricity dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment10ExposureYes())){
            error = "Failed to wait for 10. Exposure to electricity - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment10ExposureYes())){
            error = "Failed to click on 10. Exposure to electricity - Yes";
            return false;
        }
        
        //11. Exposure to moving parts of equipment including conveyors
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment11ExposureDD())){
            error = "Failed to wait for 11. Exposure to moving parts of equipment including conveyors dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment11ExposureDD())){
            error = "Failed to click on 11. Exposure to moving parts of equipment including conveyors dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment11ExposureYes())){
            error = "Failed to wait for 11. Exposure to moving parts of equipment including conveyors - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment11ExposureYes())){
            error = "Failed to click on 11. Exposure to moving parts of equipment including conveyors - Yes";
            return false;
        }
        
        //12. Failure of or working on structure (steel, concrete etc)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment12FailureDD())){
            error = "Failed to wait for 12. Failure of or working on structure (steel, concrete etc) dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment12FailureDD())){
            error = "Failed to click on 12. Failure of or working on structure (steel, concrete etc) dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment12FailureYes())){
            error = "Failed to wait for 12. Failure of or working on structure (steel, concrete etc) - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment12FailureYes())){
            error = "Failed to click on 12. Failure of or working on structure (steel, concrete etc) - Yes";
            return false;
        }
        
        //13. Fall of ground
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment13FallDD())){
            error = "Failed to wait for 13. Fall of ground dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment13FallDD())){
            error = "Failed to click on 13. Fall of ground dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment13FallYes())){
            error = "Failed to wait for 13. Fall of ground - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment13FallYes())){
            error = "Failed to click on 13. Fall of ground - Yes";
            return false;
        }
        
        //14. Fires
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment14FiresDD())){
            error = "Failed to wait for 14. Fires dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment14FiresDD())){
            error = "Failed to click on 14. Fires dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment14FiresYes())){
            error = "Failed to wait for 14. Fires - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment14FiresYes())){
            error = "Failed to click on 14. Fires - Yes";
            return false;
        }
        
        //15. Exposure to flooding
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment15ExposureDD())){
            error = "Failed to wait for 15. Exposure to flooding dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment15ExposureDD())){
            error = "Failed to click on 15. Exposure to flooding dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment15ExposureYes())){
            error = "Failed to wait for 15. Exposure to flooding - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment15ExposureYes())){
            error = "Failed to click on 15. Exposure to flooding - Yes";
            return false;
        }
        
        //16. Gas cutting and welding
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment16GasDD())){
            error = "Failed to wait for 16. Gas cutting and welding dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment16GasDD())){
            error = "Failed to click on 16. Gas cutting and welding dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment16GasYes())){
            error = "Failed to wait for 16. Gas cutting and welding - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment16GasYes())){
            error = "Failed to click on 16. Gas cutting and welding - Yes";
            return false;
        }
        
        //17. Hazardous chemicals
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment17HazardousDD())){
            error = "Failed to wait for 17. Hazardous chemicals dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment17HazardousDD())){
            error = "Failed to click on 17. Hazardous chemicals dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment17HazardousYes())){
            error = "Failed to wait for 17. Hazardous chemicals - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment17HazardousYes())){
            error = "Failed to click on 17. Hazardous chemicals - Yes";
            return false;
        }
        
        //18. Hoisting, lifting & cranage (cheery picker; forklift Incl)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment18HoistingDD())){
            error = "Failed to wait for 18. Hoisting, lifting & cranage (cheery picker; forklift Incl) dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment18HoistingDD())){
            error = "Failed to click on 18. Hoisting, lifting & cranage (cheery picker; forklift Incl) dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment18HoistingYes())){
            error = "Failed to wait for 18. Hoisting, lifting & cranage (cheery picker; forklift Incl) - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment18HoistingYes())){
            error = "Failed to click on 18. Hoisting, lifting & cranage (cheery picker; forklift Incl) - Yes";
            return false;
        }
        
        //19. Exposure to electricity including switching
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment19ExposureDD())){
            error = "Failed to wait for 19. Exposure to electricity including switching dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment19ExposureDD())){
            error = "Failed to click on 19. Exposure to electricity including switching dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment19ExposureYes())){
            error = "Failed to wait for 19. Exposure to electricity including switching - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment19ExposureYes())){
            error = "Failed to click on 19. Exposure to electricity including switching - Yes";
            return false;
        }
        
        pause(1000);
        
        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_PageObjects.Riskassessment20IlluminationLabel())){
            error = "Failed to click on 20. Illumination (during night time) dropdown";
            return false;
        }
        
        pause(1000);
        
        //20. Illumination (during night time)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment20IlluminationDD())){
            error = "Failed to wait for 20. Illumination (during night time) dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment20IlluminationDD())){
            error = "Failed to click on 20. Illumination (during night time) dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment20IlluminationYes())){
            error = "Failed to wait for 20. Illumination (during night time) - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment20IlluminationYes())){
            error = "Failed to click on 20. Illumination (during night time) - Yes";
            return false;
        }
        
        //21. Dams including tailings, storage, sewerage, stormwater and/or Inundation of water
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment21DamsDD())){
            error = "Failed to wait for 21. Dams including tailings, storage, sewerage, stormwater and/or Inundation of water dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment21DamsDD())){
            error = "Failed to click on 21. Dams including tailings, storage, sewerage, stormwater and/or Inundation of water dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment21DamsYes())){
            error = "Failed to wait for 21. Dams including tailings, storage, sewerage, stormwater and/or Inundation of water - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment21DamsYes())){
            error = "Failed to click on 21. Dams including tailings, storage, sewerage, stormwater and/or Inundation of water - Yes";
            return false;
        }
        
        //22. Material handling (manually)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment22MaterialDD())){
            error = "Failed to wait for 22. Material handling (manually) dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment22MaterialDD())){
            error = "Failed to click on 22. Material handling (manually) dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment22MaterialYes())){
            error = "Failed to wait for 22. Material handling (manually) - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment22MaterialYes())){
            error = "Failed to click on 22. Material handling (manually) - Yes";
            return false;
        }
        
        //23. Mobile machinery & pedestrian interaction
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment23MobileDD())){
            error = "Failed to wait for 23. Mobile machinery & pedestrian interaction dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment23MobileDD())){
            error = "Failed to click on 23. Mobile machinery & pedestrian interaction dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment23MobileYes())){
            error = "Failed to wait for 23. Mobile machinery & pedestrian interaction - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment23MobileYes())){
            error = "Failed to click on 23. Mobile machinery & pedestrian interaction - Yes";
            return false;
        }
        
        //24. Noise exposure
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment24NoiseDD())){
            error = "Failed to wait for 24. Noise exposure dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment24NoiseDD())){
            error = "Failed to click on 24. Noise exposure dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment24NoiseYes())){
            error = "Failed to wait for 24. Noise exposure - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment24NoiseYes())){
            error = "Failed to click on 24. Noise exposure - Yes";
            return false;
        }
        
        //25. Stored energy
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment25StoredDD())){
            error = "Failed to wait for 25. Stored energy dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment25StoredDD())){
            error = "Failed to click on 25. Stored energy dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment25StoredYes())){
            error = "Failed to wait for 25. Stored energy - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment25StoredYes())){
            error = "Failed to click on 25. Stored energy - Yes";
            return false;
        }
        
        //26. Transport operations (loading, hauling and dumping)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment26TransportDD())){
            error = "Failed to wait for 26. Transport operations (loading, hauling and dumping) dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment26TransportDD())){
            error = "Failed to click on 26. Transport operations (loading, hauling and dumping) dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment26TransportYes())){
            error = "Failed to wait for 26. Transport operations (loading, hauling and dumping) - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment26TransportYes())){
            error = "Failed to click on 26. Transport operations (loading, hauling and dumping) - Yes";
            return false;
        }
        
        //27. Working at heights
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment27WorkingDD())){
            error = "Failed to wait for 27. Working at heights dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment27WorkingDD())){
            error = "Failed to click on 27. Working at heights dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment27WorkingYes())){
            error = "Failed to wait for 27. Working at heights - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment27WorkingYes())){
            error = "Failed to click on 27. Working at heights - Yes";
            return false;
        }
        
        //28. Working in conveyor drives
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment28WorkingDD())){
            error = "Failed to wait for 28. Working in conveyor drives dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment28WorkingDD())){
            error = "Failed to click on 28. Working in conveyor drives dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment28WorkingYes())){
            error = "Failed to wait for 28. Working in conveyor drives - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment28WorkingYes())){
            error = "Failed to click on 28. Working in conveyor drives - Yes";
            return false;
        }
        
        //29. Carrying/moving compact loads
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment29CarryingDD())){
            error = "Failed to wait for 29. Carrying/moving compact loads dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment29CarryingDD())){
            error = "Failed to click on 29. Carrying/moving compact loads dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment29CarryingYes())){
            error = "Failed to wait for 29. Carrying/moving compact loads - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment29CarryingYes())){
            error = "Failed to click on 29. Carrying/moving compact loads - Yes";
            return false;
        }
        
        //30. Electrical and hand tools (grinders, drills, etc)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment30ElectricalDD())){
            error = "Failed to wait for 30. Electrical and hand tools (grinders, drills, etc) dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment30ElectricalDD())){
            error = "Failed to click on 30. Electrical and hand tools (grinders, drills, etc) dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment30ElectricalYes())){
            error = "Failed to wait for 30. Electrical and hand tools (grinders, drills, etc) - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment30ElectricalYes())){
            error = "Failed to click on 30. Electrical and hand tools (grinders, drills, etc) - Yes";
            return false;
        }
        
        //31. General construction (civil)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment31GeneralDD())){
            error = "Failed to wait for 31. General construction (civil) dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment31GeneralDD())){
            error = "Failed to click on 31. General construction (civil) dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment31GeneralYes())){
            error = "Failed to wait for 31. General construction (civil) - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment31GeneralYes())){
            error = "Failed to click on 31. General construction (civil) - Yes";
            return false;
        }
        
        //32. Highwall  & slope stability including caving
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment32HighwallDD())){
            error = "Failed to wait for 32. Highwall  & slope stability including caving dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment32HighwallDD())){
            error = "Failed to click on 32. Highwall  & slope stability including caving dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment32HighwallYes())){
            error = "Failed to wait for 32. Highwall  & slope stability including caving - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment32HighwallYes())){
            error = "Failed to click on 32. Highwall  & slope stability including caving - Yes";
            return false;
        }
        
        pause(1000);
        
        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_PageObjects.Riskassessment33EmergencyLabel())){
            error = "Failed to click on 33. Emergency call out dropdown";
            return false;
        }
        
        pause(1000);
        
        //33. Emergency call out
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment33EmergencyDD())){
            error = "Failed to wait for 33. Emergency call out dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment33EmergencyDD())){
            error = "Failed to click on 33. Emergency call out dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment33EmergencyYes())){
            error = "Failed to wait for 33. Emergency call out - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment33EmergencyYes())){
            error = "Failed to click on 33. Emergency call out - Yes";
            return false;
        }
        
        //34. Utilising sub-business partners
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment34UtilisingDD())){
            error = "Failed to wait for 34. Utilising sub-business partners dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment34UtilisingDD())){
            error = "Failed to click on 34. Utilising sub-business partners dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment34UtilisingYes())){
            error = "Failed to wait for 34. Utilising sub-business partners - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment34UtilisingYes())){
            error = "Failed to click on 34. Utilising sub-business partners - Yes";
            return false;
        }
        
        //35. Toxic fumes and gasses (CO gas)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment35ToxicDD())){
            error = "Failed to wait for 35. Toxic fumes and gasses (CO gas) dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment35ToxicDD())){
            error = "Failed to click on 35. Toxic fumes and gasses (CO gas) dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment35ToxicYes())){
            error = "Failed to wait for 35. Toxic fumes and gasses (CO gas) - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment35ToxicYes())){
            error = "Failed to click on 35. Toxic fumes and gasses (CO gas) - Yes";
            return false;
        }
        
        //36. Electric arc welding, cutting and coughing
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment36ElectricDD())){
            error = "Failed to wait for 36. Electric arc welding, cutting and coughing dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment36ElectricDD())){
            error = "Failed to click on 36. Electric arc welding, cutting and coughing dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment36ElectricYes())){
            error = "Failed to wait for 36. Electric arc welding, cutting and coughing - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment36ElectricYes())){
            error = "Failed to click on 36. Electric arc welding, cutting and coughing - Yes";
            return false;
        }
        
        //37. Working on scaffolding/ scaffold erecting or dismantling
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment37WorkingDD())){
            error = "Failed to wait for 37. Working on scaffolding/ scaffold erecting or dismantling dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment37WorkingDD())){
            error = "Failed to click on 37. Working on scaffolding/ scaffold erecting or dismantling dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment37WorkingYes())){
            error = "Failed to wait for 37. Working on scaffolding/ scaffold erecting or dismantling - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment37WorkingYes())){
            error = "Failed to click on 37. Working on scaffolding/ scaffold erecting or dismantling - Yes";
            return false;
        }
        
        //38. Oxygen fuel welding, heating and thermal cutting
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment38OxygenDD())){
            error = "Failed to wait for 38. Oxygen fuel welding, heating and thermal cutting dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment38OxygenDD())){
            error = "Failed to click on 38. Oxygen fuel welding, heating and thermal cutting dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment38OxygenYes())){
            error = "Failed to wait for 38. Oxygen fuel welding, heating and thermal cutting - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment38OxygenYes())){
            error = "Failed to click on 38. Oxygen fuel welding, heating and thermal cutting - Yes";
            return false;
        }
        
        //39. Exposure to heat stress
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment39ExposureDD())){
            error = "Failed to wait for 39. Exposure to heat stress dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment39ExposureDD())){
            error = "Failed to click on 39. Exposure to heat stress dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment39ExposureYes())){
            error = "Failed to wait for 39. Exposure to heat stress - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment39ExposureYes())){
            error = "Failed to click on 39. Exposure to heat stress - Yes";
            return false;
        }
        
        //40. Winches
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment40WinchesDD())){
            error = "Failed to wait for 40. Winches dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment40WinchesDD())){
            error = "Failed to click on 40. Winches dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment40WinchesYes())){
            error = "Failed to wait for 40. Winches - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment40WinchesYes())){
            error = "Failed to click on 40. Winches - Yes";
            return false;
        }
        
        //41. Trackbound/railbound activities
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment41TrackboundDD())){
            error = "Failed to wait for 41. Trackbound/railbound activities dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment41TrackboundDD())){
            error = "Failed to click on 41. Trackbound/railbound activities dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment41TrackboundYes())){
            error = "Failed to wait for 41. Trackbound/railbound activities - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment41TrackboundYes())){
            error = "Failed to click on 41. Trackbound/railbound activities - Yes";
            return false;
        }
        
        //42. Exposure to hot materials
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment42ExposureDD())){
            error = "Failed to wait for 42. Exposure to hot materials dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment42ExposureDD())){
            error = "Failed to click on 42. Exposure to hot materials dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment42ExposureYes())){
            error = "Failed to wait for 42. Exposure to hot materials - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment42ExposureYes())){
            error = "Failed to click on 42. Exposure to hot materials - Yes";
            return false;
        }
        
        //43. Exposure to molten metals
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment43ExposureDD())){
            error = "Failed to wait for 43. Exposure to molten metals dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment43ExposureDD())){
            error = "Failed to click on 43. Exposure to molten metals dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment43ExposureYes())){
            error = "Failed to wait for 43. Exposure to molten metals - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment43ExposureYes())){
            error = "Failed to click on 43. Exposure to molten metals - Yes";
            return false;
        }
        
        pause(1000);
        
        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_PageObjects.Riskassessment44UsageLabel())){
            error = "Failed to click on 44. Usage of power tools dropdown";
            return false;
        }
        
        pause(1000);
        
        //44. Usage of power tools
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment44UsageDD())){
            error = "Failed to wait for 44. Usage of power tools dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment44UsageDD())){
            error = "Failed to click on 44. Usage of power tools dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment44UsageYes())){
            error = "Failed to wait for 44. Usage of power tools - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment44UsageYes())){
            error = "Failed to click on 44. Usage of power tools - Yes";
            return false;
        }
        
        //45. Exposure to radioactive sources
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment45ExposureDD())){
            error = "Failed to wait for 45. Exposure to radioactive sources dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment45ExposureDD())){
            error = "Failed to click on 45. Exposure to radioactive sources dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment45ExposureYes())){
            error = "Failed to wait for 45. Exposure to radioactive sources - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment45ExposureYes())){
            error = "Failed to click on 45. Exposure to radioactive sources - Yes";
            return false;
        }
        
        //46. Working at uneven/slippery surfaces
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment46WorkingDD())){
            error = "Failed to wait for 46. Working at uneven/slippery surfaces dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment46WorkingDD())){
            error = "Failed to click on 46. Working at uneven/slippery surfaces dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment46WorkingYes())){
            error = "Failed to wait for 46. Working at uneven/slippery surfaces - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment46WorkingYes())){
            error = "Failed to click on 46. Working at uneven/slippery surfaces - Yes";
            return false;
        }
        
        //47. Inadequate or lack of ventilation
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment47InadequateDD())){
            error = "Failed to wait for 47. Inadequate or lack of ventilation dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment47InadequateDD())){
            error = "Failed to click on 47. Inadequate or lack of ventilation dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment47InadequateYes())){
            error = "Failed to wait for 47. Inadequate or lack of ventilation - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment47InadequateYes())){
            error = "Failed to click on 47. Inadequate or lack of ventilation - Yes";
            return false;
        }
        
        //48. Transportation of people
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment48TransportationDD())){
            error = "Failed to wait for 48. Transportation of people dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment48TransportationDD())){
            error = "Failed to click on 48. Transportation of people dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment48TransportationYes())){
            error = "Failed to wait for 48. Transportation of people - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment48TransportationYes())){
            error = "Failed to click on 48. Transportation of people - Yes";
            return false;
        }
        
        //49. Performing IT cabling
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment49PerformingDD())){
            error = "Failed to wait for 49. Performing IT cabling dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment49PerformingDD())){
            error = "Failed to click on 49. Performing IT cabling dropdown";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Riskassessment49PerformingYes())){
            error = "Failed to wait for 49. Performing IT cabling - Yes";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Riskassessment49PerformingYes())){
            error = "Failed to click on 49. Performing IT cabling - Yes";
            return false;
        }
        
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully selected Yes for all Answers and 49 visible in the Total field");
        
        //Likelihood of risk exposure criteria
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentLikelihoodriskexposurecriteriaDD())){
            error = "Failed to wait for Likelihood of risk exposure criteria dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentLikelihoodriskexposurecriteriaDD())){
            error = "Failed to click on Likelihood of risk exposure criteria dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentLikelihoodriskexposurecriteriaSelect(getData("Likelihood of risk exposure criteria")))){
            error = "Failed to wait for Likelihood of risk exposure criteria option.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentLikelihoodriskexposurecriteriaSelect(getData("Likelihood of risk exposure criteria")))){
            error = "Failed to click on Likelihood of risk exposure criteria option.";
            return false; 
        }
        
        //Number of employees
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentNoemployeesDD())){
            error = "Failed to wait for Number of employees dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentNoemployeesDD())){
            error = "Failed to click on Number of employees dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentNoemployeesSelect(getData("Number of employees")))){
            error = "Failed to wait for Number of employees option.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentNoemployeesSelect(getData("Number of employees")))){
            error = "Failed to click on Number of employees option.";
            return false; 
        }
        
        //Duration of work
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentDurationworkDD())){
            error = "Failed to wait for Number of employees dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentDurationworkDD())){
            error = "Failed to click on Number of employees dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentDurationworkDDSelect(getData("Duration of work")))){
            error = "Failed to wait for Number of employees option.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentDurationworkDDSelect(getData("Duration of work")))){
            error = "Failed to click on Number of employees option.";
            return false; 
        }
        
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully selected optiond for Likelihood of risk exposure criteria, Number of employees and Duration of work fields and the Total Score Formula field with the calculated value");
        
        //Risk Assessment Save Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentSave())){
            error = "Failed to wait for Risk Assessment Save Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentSave())){
            error = "Failed to click on Risk Assessment Save Button";
            return false;
        }
        
        pause(15000);
        
        //Process Flow Edit phase
        narrator.stepPassedWithScreenShot("Processflow in Edit phase");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatusChild("Edit phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Edit phase";
        return false;
        }
        
        return true;
    }
}
