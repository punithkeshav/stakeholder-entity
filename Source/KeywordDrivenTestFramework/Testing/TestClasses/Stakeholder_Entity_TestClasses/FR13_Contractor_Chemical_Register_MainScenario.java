/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.previousDate;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsometricsPOCPageObjects;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Contractor Chemical Register - Main Scenario",
        createNewBrowserInstance = false
)

public class FR13_Contractor_Chemical_Register_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR13_Contractor_Chemical_Register_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 90);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
 
    }

    public TestResult executeTest()
    {
        if (!navigateToChemicalRegister()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterChemicalRegisterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        if(!openChemicalsContractorwillbeexposedto()){
            return narrator.testFailed("Failed due - " + error);
        }
       
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToChemicalRegister(){    
        //Navigate to Tools and Equipment tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_ChemicalRegistertab())){
            error = "Failed to wait for Chemical Register tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_ChemicalRegistertab())){
            error = "Failed to click on Chemical Register tab";
            return false;
        }
        SeleniumDriverInstance.pause(10000);
        narrator.stepPassedWithScreenShot("Successfully navigated to Chemical Register tab");

        return true;
    }
    
    public boolean enterChemicalRegisterDetails(){

        //Chemicals they bring onto site
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Chemicalstheybringontosite())){
            error = "Failed to wait for Chemicals they bring onto site";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Chemicalstheybringontosite())){
            error = "Failed to click on Chemicals they bring onto site";
            return false;
        }
        
        pause(2000);

        return true;
        
    }
    
    public boolean openChemicalsContractorwillbeexposedto(){

        //Chemicals Contractor will be exposed to
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ChemicalsContractorwillbeexposedto())){
            error = "Failed to wait for Chemicals Contractor will be exposed to";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ChemicalsContractorwillbeexposedto())){
            error = "Failed to click on Chemicals Contractor will be exposed to";
            return false;
        }
        
        pause(15000);
        
        //Hazardous Chemical Register Record close
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.HazardousChemicalRegisterRecordClosed())){
            error = "Failed to wait for Chemicals Contractor will be exposed to";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.HazardousChemicalRegisterRecordClosed())){
            error = "Failed to click on Chemicals Contractor will be exposed to";
            return false;
        }
        
        pause(4000);
        
        narrator.stepPassedWithScreenShot("Successfully Updated Chemical Register details, opened and closed Chemicals Contractor will be exposed to record ");
        
        return true;
    }

}
