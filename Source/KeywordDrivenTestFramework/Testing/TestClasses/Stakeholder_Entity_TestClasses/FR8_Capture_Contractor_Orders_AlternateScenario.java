/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.previousDate;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Contractor Orders - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR8_Capture_Contractor_Orders_AlternateScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR8_Capture_Contractor_Orders_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
 
    }

    public TestResult executeTest()
    {
        if (!navigateToContractorSupplierManager()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterOrdersDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
       
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToContractorSupplierManager(){    
//        //Navigate to Contractor Or Supplier Manager Tab
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.contractorSupplierManager_Tab())){
//            error = "Failed to wait for 'Contractor Or Supplier Manager' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.contractorSupplierManager_Tab())){
//            error = "Failed to click on 'Contractor Or Supplier Manager' tab.";
//            return false;
//        }
//        SeleniumDriverInstance.pause(2000);
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Contractor Or Supplier Manager' tab.");
        
        if(!SeleniumDriverInstance.scrollToElement(Stakeholder_Entity_PageObjects.questionnaires_Panel())){
            error = "Failed to click on 'Questionnaires' panel.";
            return false;
        }
        
        pause(2000);
        
        //Navigate to Orders panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Panel())){
            error = "Failed to wait for 'Questionnaires' panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_Panel())){
            error = "Failed to click on 'Questionnaires' panel.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Orders' panel.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Orders Add' button.");
        
        pause(3000);
        
        return true;
    }
    
    //Enter data
    public boolean enterOrdersDetails(){
        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_PF())){
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_PF())){
            error = "Failed to click on Process Flow button.";
            return false;
        }
        
        //Process Flow Add phase
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatusChild("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Order number
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_OrderNo())){
            error = "Failed to wait for 'Order number' field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Orders_OrderNo(), getData("Order number"))){
            error = "Failed to click on '" + getData("Order number") + "' field.";
            return false; 
        }
        
        //Order Status
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_StatusDD())){
            error = "Failed to wait for 'Order Status' dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_StatusDD())){
            error = "Failed to click on 'Order Status' dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Status_Dropdown_Option(getData("Order Status")))){
            error = "Failed to wait for '" + getData("Order Status") + "' option.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_Status_Dropdown_Option(getData("Order Status")))){
            error = "Failed to click on '" + getData("Order Status") + "' into 'Order Status' option.";
            return false; 
        }
        
//        //Approved By
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_ApprovedbyDD())){
//            error = "Failed to wait for 'Approved By' dropdown.";
//            return false; 
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_ApprovedbyDD())){
//            error = "Failed to click on 'Approved By' dropdown.";
//            return false; 
//        }
//        
//        pause(2000);
//        
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Approvedby_Dropdown_Option())){
//            error = "Failed to wait for Approved By option";
//            return false; 
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_Approvedby_Dropdown_Option())){
//            error = "Failed to click on Approved By option";
//            return false; 
//        }
//        
//        //Date approved
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_dateapproved())){
//            error = "Failed to wait for 'Date approved' field.";
//            return false; 
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Orders_dateapproved(), getData("Date approved"))){
//            error = "Failed to click on '" + getData("Date approved") + "' field.";
//            return false; 
//        }
        
        //Scope of Work
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Scopeofworktab())){
            error = "Failed to wait for 'Scope of Work' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_Scopeofworktab())){
            error = "Failed to click on 'Scope of Work' tab.";
            return false;
        }
        
        pause(2000);
        
        //Area where work conducted
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.MultiselectexpandChevronTree(testData.getData("Business Unit Expand1"),"1"))){
            error = "Failed to wait for Area where work conducted expandChevronTree1";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.MultiselectexpandChevronTree(testData.getData("Business Unit Expand1"),"1"))){
            error = "Failed to click on Area where work conducted expandChevronTree1";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.MultiselectexpandChevronTree(testData.getData("Business Unit Expand2"),"1"))){
            error = "Failed to wait for Area where work conducted expandChevronTree2";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.MultiselectexpandChevronTree(testData.getData("Business Unit Expand2"),"1"))){
            error = "Failed to click on Area where work conducted expandChevronTree2";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.MultiselectSelect(testData.getData("Business Unit Tick"),"1"))){
            error = "Failed to wait for Area where work conducted expandChevronTree2";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.MultiselectSelect(testData.getData("Business Unit Tick"),"1"))){
            error = "Failed to click on Area where work conducted expandChevronTree2";
            return false;
        }
        
        //Project
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Project())){
            error = "Failed to wait for 'Project' field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Orders_Project(), getData("Project"))){
            error = "Failed to click on '" + getData("Project") + "' field.";
            return false; 
        }
        //Project description
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Projectdescription())){
            error = "Failed to wait for 'Project description' field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Orders_Projectdescription(), getData("Project description"))){
            error = "Failed to click on '" + getData("Project description") + "' field.";
            return false; 
        }
        
        //Start date
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Startdate())) {
            error = "Failed to wait for 'Start date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Orders_Startdate(), startDate)) {
            error = "Failed to enter '" + startDate + "' into Start date.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + startDate + "' into Start date field.");
        
        //End date
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Enddate())) {
            error = "Failed to wait for 'Start date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Orders_Enddate(), endDate)) {
            error = "Failed to enter '" + endDate + "' into Start date.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + endDate + "' into End date field.");
        
        //Contractor type
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_ContractortypeDD())){
            error = "Failed to wait for 'Contractor type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_ContractortypeDD())){
            error = "Failed to click on 'Contractor type' dropdown.";
            return false;
        }

        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Contractortype_Option(getData("Contractor type")))){
            error = "Failed to wait for '" + getData("Contractor type") + "' option.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_Contractortype_Option(getData("Contractor type")))){
            error = "Failed to click on '" + getData("Contractor type") + "' option.";
            return false; 
        }
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_Save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
      
        pause(20000);
        
        //Process Flow Add phase
        narrator.stepPassedWithScreenShot("Processflow in Edit phase");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatusChild("Edit phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Edit phase";
        return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully saved contractor Orders.");
        
        return true;
    }
}

