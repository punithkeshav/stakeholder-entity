/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;
/**
 *
 * @author Delwin.Horsthemke
 */
@KeywordAnnotation(
        Keyword = "Capture Stakeholder Entity Information - Community",
        createNewBrowserInstance = false
)

public class UC_STE_01_02_StakeholderEntityAdditionalDetails_Community extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public UC_STE_01_02_StakeholderEntityAdditionalDetails_Community()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToEntityInformation())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToEntityInformation(){
        //Left Arrow
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.leftArrowTab())){
//            error = "Failed to wait for 'Left' Arrow.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Entity_PageObjects.leftArrowTab())){
//            error = "Failed to click on 'Left' Arrow.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked 'Left' Arrow.");
        
        String SE_SocialStatusPanel;
        SE_SocialStatusPanel = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.SE_SocialStatusPanel());
        
        //Social Status
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_SocialStatusPanel())){
            error = "Failed to wait for 'Social Status' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_SocialStatusPanel())){
            error = "Failed to click on 'Social Status' button.";
            return false;
        }
        
        pause(2000);

        //Navigate to Stakeholder Details Tab
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.entityInformationTab())){
            error = "Failed to wait for 'Stakeholder Details' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.entityInformationTab())){
            error = "Failed to click on 'Stakeholder Details' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Entity Information' tab.");
        
        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
        //Phone number
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.primaryContactNo())){
            error = "Failed to wait for 'Phone number' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.primaryContactNo(), getData("Phone number"))){
            error = "Failed to enter '" + getData("Phone number") + "' into 'Primary contact number' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Phone number: '" + getData("Primary Contact No") + "' .");
        
        //Secondary contact number
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.secondaryContactNo())){
//            error = "Failed to wait for 'Secondary contact number' input.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.secondaryContactNo(), getData("Secondary Contact No"))){
//            error = "Failed to enter '" + getData("Secondary Contact No") + "' into 'Secondary contact number' input.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Secondary contact number: '" + getData("Secondary Contact No") + "' .");
        

        //Email Address
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.emailAddress())){
            error = "Failed to wait for 'Email Address' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.emailAddress(), getData("Email Address"))){
            error = "Failed to enter '" + getData("Email Address") + "' into 'Email Address' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Email Address : '" + getData("Email Address") + "' .");
        
        // Address
        //Street 1
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.streetOne())){
            error = "Failed to wait for 'Street 1' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.streetOne(), getData("Street 1"))){
            error = "Failed to enter '" + getData("Street 1") + "' into 'Street 1' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Street 1 : '" + getData("Street 1") + "' .");
        
         //Street 2
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.streetTwo())){
            error = "Failed to wait for 'Street 2' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.streetTwo(), getData("Street 2"))){
            error = "Failed to enter '" + getData("Street 2") + "' into 'Street 2' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Street 2 : '" + getData("Street 2") + "' .");
        
        //Zip /Postal code
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_PostalCode())){
            error = "Failed to wait for 'Postal code' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.se_PostalCode(), testData.getData("Postal code"))){
            error = "Failed to click 'Postal code' input field.";
            return false;
        } 
        
        //Location
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_Location())){
            error = "Failed to wait for 'Location' input field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.se_Location())){
            error = "Failed to click  'Location' input field.";
            return false;
        }
        
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.se_Location_select(testData.getData("Location")))){
            error = "Failed to wait for Location dropdown option.";
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.se_Location_select(testData.getData("Location")))){
            error = "Failed to click Location dropdown option.";
            return false;
        }
         
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        pause(15000);
        
        narrator.stepPassedWithScreenShot("Successfully added Entity Information for Community Stakeholder");
        
        return true;
        
    }
}
