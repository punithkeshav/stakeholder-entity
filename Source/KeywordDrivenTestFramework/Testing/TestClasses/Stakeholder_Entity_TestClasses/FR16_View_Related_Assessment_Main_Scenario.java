/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_View_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.JavascriptExecutor;
/**
 *
 * @author Delwin.Horsthemke
 */
@KeywordAnnotation(
        Keyword = "View Related Assessment - Main Scenario",
        createNewBrowserInstance = false
)

public class FR16_View_Related_Assessment_Main_Scenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR16_View_Related_Assessment_Main_Scenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderEntity())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToStakeholderEntity(){
        
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to wait for 'Stakeholders Entity' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to click on 'Stakeholders Entity' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Entity' tab.");
        
        pause(5000);
        
        //Entity Name Search
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_View_PageObjects.SE_EntityNameSearchBox(), testData.getData("SE_EntityNameSearch"))){
            error = "Failed to enter '"+testData.getData("SE_EntityNameSearch")+"' into 'SE_EntityNameSearch' input.";
            return false;
        }
        
        //Search Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to wait for Search Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.searchbutton())){
            error = "Failed to click on Search Button";
            return false;
        }
        
        SeleniumDriverInstance.pause(6000);
        narrator.stepPassedWithScreenShot("Successfully clicked on Search Button");
        
        //Stakeholder Selection
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.SE_RecordSelection())){
            error = "Failed to wait for Stakeholder Selection";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.SE_RecordSelection())){
            error = "Failed to click on Stakeholder Selection";
            return false;
        }
        
        pause(10000);
        
        //Right Arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.rightArrow())){
            error = "Failed to wait for 'Left' Arrow.";
            return false;
        }
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Entity_View_PageObjects.rightArrow())){
            error = "Failed to click on 'Left' Arrow.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked 'Left' Arrow.");
        
        //Related Assessment tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.relatedAssessmentTab())){
            error = "Failed to wait for Related Assessment tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.relatedAssessmentTab())){
            error = "Failed to click on Related Assessment tab";
            return false;
        }

        SeleniumDriverInstance.pause(20000);
        
        //Related Inspections: Record to be opened
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_View_PageObjects.inspectionsRecordToBeOpened())){
            error = "Failed to wait for Related Inspections: Record to be opened";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_View_PageObjects.inspectionsRecordToBeOpened())){
            error = "Failed to click on Related Inspections: Record to be opened";
            return false;
        }
        
         pause(15000);
        
        narrator.stepPassedWithScreenShot("Successfully opened Inspections record");
        
        return true;
    }

}
