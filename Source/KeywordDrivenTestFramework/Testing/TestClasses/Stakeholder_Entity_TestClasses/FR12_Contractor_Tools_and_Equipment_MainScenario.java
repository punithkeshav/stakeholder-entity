/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.previousDate;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsometricsPOCPageObjects;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Contractor Tools and Equipment - Main Scenario",
        createNewBrowserInstance = false
)

public class FR12_Contractor_Tools_and_Equipment_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR12_Contractor_Tools_and_Equipment_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 90);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
 
    }

    public TestResult executeTest()
    {
        if (!navigateToToolsandEquipment()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterToolsandEquipmentDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
       
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToToolsandEquipment(){    
        //Navigate to Tools and Equipment tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_ToolsandEquipmenttab())){
            error = "Failed to wait for Tools and Equipment tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_ToolsandEquipmenttab())){
            error = "Failed to click on Tools and Equipment tab";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        narrator.stepPassedWithScreenShot("Successfully navigated to Tools and Equipment tab");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_ToolsandEquipmentadd())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_ToolsandEquipmentadd())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Tools and Equipment Add' button.");

        return true;
    }
    
    public boolean enterToolsandEquipmentDetails(){
        
        pause(5000);

        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ToolsandEquipmentPF())){
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ToolsandEquipmentPF())){
            error = "Failed to click on Process Flow button.";
            return false;
        }
        
        //Process Flow Add phase
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatusChild("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Related Documents
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ToolsandEquipmentRelatedDocsTab())){
            error = "Failed to wait for Related Documents tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ToolsandEquipmentRelatedDocsTab())){
            error = "Failed to click on Related Documents tab.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ToolsandEquipmentDocsLinkDoc()))
        {
            error = "Failed to locate Tools and Equipment Supporting Documents Link Document";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ToolsandEquipmentDocsLinkDoc()))
        {
            error = "Failed to click on Tools and Equipment Supporting Documents Link Document";
            return false;
        }
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        pause(1000);
        
        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentLinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.RiskassessmentLinkURL(), getData("HTTPURL") )){
            error = "Failed to enter URL value field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link");
        
        //Upload Link Title
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmentURLTitle())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmentURLTitle())){
            error = "Failed to enter Url Title field.";
            return false;
        }
            
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.RiskassessmentURLTitle(), getData("URLTitle"))){
            error = "Failed to enter Url Title field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("URL Title");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.RiskassessmenturlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.RiskassessmenturlAddButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded document using Link");
        
        //Switch to iframe
        if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";

        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Equipment Detail tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ToolsandEquipmentEquipmentDetailTab())){
            error = "Failed to wait for Equipment Detail tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ToolsandEquipmentEquipmentDetailTab())){
            error = "Failed to click on Equipment Detail tab.";
            return false;
        }
        
        //Description of equipment
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Descriptionofequipment())){
            error = "Failed to wait for Description of equipment field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Descriptionofequipment(), getData("Description of equipment"))){
            error = "Failed to click on 'Description of equipment' field.";
            return false; 
        }
        
        //Serial number
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Serialnumber())){
            error = "Failed to wait for Serial number field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Serialnumber(), getData("Serial number"))){
            error = "Failed to click on Serial number field.";
            return false; 
        }
        
        //Calibration certificate
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Calibrationcertificate())){
            error = "Failed to wait for Calibration certificate field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Calibrationcertificate(), getData("Calibration certificate"))){
            error = "Failed to click on Calibration certificate field.";
            return false; 
        }
        
        //Load test certificate
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Loadtestcertificate())){
            error = "Failed to wait for Load test certificate field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Loadtestcertificate(), getData("Load test certificate"))){
            error = "Failed to click on Load test certificate field.";
            return false; 
        }
        
        //Equipment tagged (Ref No.)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Equipmenttagged())){
            error = "Failed to wait for Equipment tagged (Ref No.) field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Equipmenttagged(), getData("Equipment tagged (Ref No.)"))){
            error = "Failed to click on Equipment tagged (Ref No.) field.";
            return false; 
        }
        
        //Name of tagging authority
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Nameoftaggingauthority())){
            error = "Failed to wait for Name of tagging authority field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Nameoftaggingauthority(), getData("Name of tagging authority"))){
            error = "Failed to click on Name of tagging authority field.";
            return false; 
        }
        
        //Pre-use inspection checklist
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Preuseinspectionchecklist())){
            error = "Failed to wait for Pre-use inspection checklist field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Preuseinspectionchecklist(), getData("Pre-use inspection checklist"))){
            error = "Failed to click on Pre-use inspection checklist field.";
            return false; 
        }
        
        //Logbook (Ref No.)
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Logbook())){
            error = "Failed to wait for Logbook (Ref No.) field.";
            return false; 
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.Logbook(), getData("Logbook (Ref No.)"))){
            error = "Failed to click on Logbook (Ref No.) field.";
            return false; 
        }

        //Save Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.ToolsandEquipmentEquipmentSave())){
            error = "Failed to wait for Save Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ToolsandEquipmentEquipmentSave())){
            error = "Failed to click on Save Button.";
            return false;
        }
        
        pause(15000);
        
        //Process Flow Add phase
        narrator.stepPassedWithScreenShot("Processflow in Edit phase");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatusChild("Edit phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Edit phase";
        return false;
        }
        
        return true;
        
    }
}
