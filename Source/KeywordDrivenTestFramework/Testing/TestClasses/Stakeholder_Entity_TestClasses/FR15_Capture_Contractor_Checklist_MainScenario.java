/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.previousDate;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.IsometricsPOCPageObjects;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Contractor Checklist - Main Scenario",
        createNewBrowserInstance = false
)

public class FR15_Capture_Contractor_Checklist_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR15_Capture_Contractor_Checklist_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 10);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
 
    }

    public TestResult executeTest()
    {
        if (!navigateToEvaluations()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterContractorChecklistDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
       
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToEvaluations(){    
        //Navigate to Evaluations tab
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ChildTabScrollRight())){
            error = "Failed to click on Tab Scroll Right button";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ChildTabScrollRight())){
            error = "Failed to click on Tab Scroll Right button";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.ChildTabScrollRight())){
            error = "Failed to click on Tab Scroll Right button";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Evaluationstab())){
            error = "Failed to wait for Evaluations tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_Evaluationstab())){
            error = "Failed to click on Evaluations tab";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        narrator.stepPassedWithScreenShot("Successfully navigated to Evaluations tab");
        
        //Add Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.Orders_Evaluationsadd())){
            error = "Failed to wait for Evaluations Add";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.Orders_Evaluationsadd())){
            error = "Failed to click on Evaluations Add";
            return false;
        }
        
        return true;
    }
    
    public boolean enterContractorChecklistDetails(){
        
        pause(5000);
        
        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.contractor_checklistPF())){
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.contractor_checklistPF())){
            error = "Failed to click on Process Flow button.";
            return false;
        }
        
        //Process Flow Add phase
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatusChild("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Start date
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.contractor_checklistStartDate())) {
            error = "Failed to wait for Start date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.contractor_checklistStartDate(), startDate)) {
            error = "Failed to enter '" + startDate + "' into Start date date";
            return false;
        }
        
        //End date
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.contractor_checklistEndDate())) {
            error = "Failed to wait for End date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.contractor_checklistEndDate(), endDate)) {
            error = "Failed to enter '" + endDate + "' into End date date";
            return false;
        }
        
        //Checklist
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.contractor_checklistDD())){
            error = "Failed to wait for Checklist dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.contractor_checklistDD())){
            error = "Failed to click on Checklist dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.contractor_checklist_Option(getData("Checklist")))){
            error = "Failed to wait for '" + getData("Checklist") + "' option.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.contractor_checklist_Option(getData("Checklist")))){
            error = "Failed to click on '" + getData("Checklist") + "' into 'Order Status' option.";
            return false; 
        }
        
        //Person conducting the checklist
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.checklist_PersonconductingchecklistDD())){
            error = "Failed to wait for Person conducting the checklist dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.checklist_PersonconductingchecklistDD())){
            error = "Failed to click on Person conducting the checklist dropdown.";
            return false; 
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.checklist_PersonconductingchecklistOption(getData("Person conducting the checklist")))){
            error = "Failed to wait for '" + getData("Person conducting the checklist") + "' option.";
            return false; 
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.checklist_PersonconductingchecklistOption(getData("Person conducting the checklist")))){
            error = "Failed to click on '" + getData("Person conducting the checklist") + "' into 'Order Status' option.";
            return false; 
        }
        
        //Save and Continue Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.checklist_SaveandContinue())){
            error = "Failed to wait for Save and Continue Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.checklist_SaveandContinue())){
            error = "Failed to click on Save and Continue Button";
            return false;
        }

        pause(15000);
        
        String checklist_StartBtn;
        checklist_StartBtn = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.checklist_StartBtn());
        
        String checklist_Findingstab;
        checklist_Findingstab = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.checklist_Findingstab());
        
        String checklist_SupportDocstab;
        checklist_SupportDocstab = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.checklist_SupportDocstab());
        
        narrator.stepPassedWithScreenShot("Successfully clicked Save and Continue button and START button / Findings & Supporting Documents Tabs visible");
        
        return true;
    }
  
}
