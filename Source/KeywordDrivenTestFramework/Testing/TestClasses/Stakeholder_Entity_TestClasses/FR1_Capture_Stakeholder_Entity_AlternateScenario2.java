/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Entity_TestClasses;

import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects.Stakeholder_Entity_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Stakeholder Entity - Alternate Scenario 2",
        createNewBrowserInstance = false
)

public class FR1_Capture_Stakeholder_Entity_AlternateScenario2 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capture_Stakeholder_Entity_AlternateScenario2()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderEntity())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToStakeholderEntity(){
       //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to wait for 'Stakeholders Entity' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.navigate_StakeholderEntity())){
            error = "Failed to click on 'Stakeholders Entity' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Entity' tab.");
        
        //Add button
        SeleniumDriverInstance.pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_add(), 2000)){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(8000);
        
        String SE_ProfileTab;
        SE_ProfileTab = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.SE_ProfileTab());
        
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button and Stakeholder Entity record opens with Profile Tab visible");

        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
        //Process flow 
        pause(1000);        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_processflow())){
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_processflow())){
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        
        //Process Flow Add phase
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatus("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Entity Type dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdown(),5000)){
            error = "Failed to wait for 'Entity Type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdown())){
            error = "Failed to click 'Entity Type' dropdown.";
            return false;
        }
        
        pause(5000);
         
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdownValue(testData.getData("EntityType"))))
        {
            error = "Failed to locate '"+testData.getData("Entity Type")+"' into 'Entity Type' input.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdownValue(testData.getData("EntityType"))))
        {
            error = "Failed to enter '"+testData.getData("Entity Type")+"' into 'Entity Type' input.";
            return false;
        }

        //Entity name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_name())){
            error = "Failed to wait for 'Entity name' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.SE_name(), testData.getData("Entity name"))){
            error = "Failed to enter '"+testData.getData("Entity name")+"' into 'Entity name' input.";
            return false;
        }
        
        //Known as
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_knownas())){
            error = "Failed to wait for 'Known as' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.SE_knownas(), testData.getData("Known as"))){
            error = "Failed to enter '"+testData.getData("Known as")+"' into 'Known as' input.";
            return false;
        }
        
        //Industry dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_industry_dropdown())){
            error = "Failed to wait for 'Industry' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_industry_dropdown())){
            error = "Failed to click 'Industry' dropdown.";
            return false;
        }
        
        //Industry select 
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_industry_select(testData.getData("Industry")))){
            error = "Failed to wait for 'Industry' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_industry_select(testData.getData("Industry")))){
            error = "Failed to select '"+testData.getData("Industry")+"' from 'Industry' dropdown options.";
            return false;
        }

        //Entity description
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_desc())){
            error = "Failed to wait for 'Entity name' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.SE_desc(), testData.getData("Entity description"))){
            error = "Failed to enter '"+testData.getData("Entity description")+"' into 'Entity description' textarea.";
            return false;
        }
        
        //Relationship owner dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_owner_dropdown())){
            error = "Failed to wait for 'Relationship owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_owner_dropdown())){
            error = "Failed to click 'Relationship owner' dropdown.";
            return false;
        }
        
        //Relationship owner select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_owner_select(testData.getData("Relationship owner")))){
            error = "Failed to wait for 'Relationship owner' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_owner_select(testData.getData("Relationship owner")))){
            error = "Failed to select '"+testData.getData("Relationship owner")+"' from 'Relationship owner' dropdown options.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Entity details completed");
        
        //Stakeholder categories
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_categories(testData.getData("Categories")))){
            error = "Failed to wait for '"+testData.getData("Categories")+"' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_categories(testData.getData("Categories")))){
            error = "Failed to click '"+testData.getData("Categories")+"' option.";
            return false;
        }
        
        //Business Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_business_unit_selectall())){
            error = "Failed to wait for 'Business unit' select all.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_business_unit_selectall())){
            error = "Failed to click 'Business unit' select all.";
            return false;
        }
        
        //Impact types
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_impact_types_selectall())){
            error = "Failed to wait for 'Impact type' select all.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_impact_types_selectall())){
            error = "Failed to click 'Impact type' select all.";
            return false;
        }

        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_savetocontinue())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_savetocontinue())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Entity_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        pause(10000);
        
        //Process Flow Edit phase
        narrator.stepPassedWithScreenShot("Processflow in Edit phase");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(Stakeholder_Entity_PageObjects.processFlowStatus("Edit phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Edit phase";
        return false;
        }
        
//        String SE_SocialStatusPanel;
//        SE_SocialStatusPanel = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.SE_SocialStatusPanel());
        
        String SE_EntityStatusActive;
        SE_EntityStatusActive = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.SE_EntityStatusActive());

        narrator.stepPassedWithScreenShot("Stakeholder Entity details added successfully and record saved, Social Status panel / Entity Status = Active and all tabs display");
        
        //Check if the record has been Saved
//        if(SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup())){
//            String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.recordSaved_popup());
//
//            if(saved.equals("Record saved")){
//                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//            }else{   
//                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.failed())){
//                    error = "Failed to wait for error message.";
//                    return false;
//                }
//
//                String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Entity_PageObjects.failed());
//
//                if(failed.equals("ERROR: Record could not be saved")){
//                    error = "Failed to save record.";
//                    return false;
//                }
//            }
//        }
        
        return true;
    }

}
