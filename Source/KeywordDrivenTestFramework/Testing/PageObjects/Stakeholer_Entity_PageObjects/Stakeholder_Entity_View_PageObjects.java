/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class Stakeholder_Entity_View_PageObjects extends BaseClass
{

    //FR16 MS
    public static String SE_EntityNameSearchBox()
    {
        return "(//div[@id='searchOptions']//input[@class='txt border'])[2]";
    }
    
    public static String SE_RecordSelection()
    {
        return "//*[@id='grid']/div[3]/table/tbody/tr[2]/td[6]/div";
    }
    
    public static String rightArrow()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }
    
     public static String relatedAssessmentTab()
    {
        return "//li[@id='tab_2A58D822-5DDA-4DE1-BA6C-32F361BF2B1C']";
    }

    public static String inspectionsRecordToBeOpened() {
        return "((//div[@id='control_65FF0BD7-91A2-46FB-9FA0-E0A4C7A0CE4D']//div//table)[3]//tr)[1]";

    }
    
    //FR17 MS
    public static String engagementsTab()
    {
        return "//li[@id='tab_3942DBC8-B12A-4A79-97F6-4ED66D494909']";
    }

    public static String engagementsRecordToBeOpened() {
        return "((//div[@id='control_89186F28-56D9-423C-9FA4-8D688243B982']//div//table)[3]//tr)[1]";

    }
    
    //FR17 AS
     public static String addEngagementsButton()
    {
        return "//div[@id='control_A50A638E-99DE-44B8-A7B5-2BF8A948862A']";
    }

     public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }
     
    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

     public static String engagementTitle()
    {
        return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
    }
     
    public static String businessUnitTab()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//li";
    }

//    public static String businessUnit_SelectAll()
//    {
//        return "(//span[@class='select3-arrow']//b[@class='select3-all'])[3]";
//    }

    public static String businessUnit_SelectAll()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//b[@original-title='Select all']";
    }

    
    public static String businessUnitSelect(String text)
    {
        return "(//div[@id='divForms']/div[5]//a[contains(text(),'" + text + "')]/i)[1]";
    }

    public static String expandGlobalCompany()
    {
        return "//a[text()='Global Company']/../i";
    }

    public static String expandSouthAfrica()
    {
        return "//a[text()='South Africa']/../i";
    }

    public static String businessUnit_GlobalCompany()
    {
        return "//a[contains(text(),'Global Company')]";
    }

    public static String specificLocation()
    {
        return "(//div[@id='control_0501A692-460C-4DB5-9EAC-F15EE8934113']//input)[1]";
    }

    public static String projectLink()
    {
        return "//div[@id='control_29AB36D5-E83F-43EF-AFF5-F7353A5353E9']/div[1]";
    }

    public static String projectTab()
    {
        return "//div[@id='control_963F5190-1317-42C1-AD7A-B277FCBA7101']";
    }

    public static String anyProject(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String cn_entity()
    {
        return "//div[text()='Create a new entity']";
    }

    public static String closeBusinessUnit()
    {
        return "(//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//span[2]//b[1])";
    }
    
    public static String engagementDate()
    {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }
    
     public static String anyEngagementMethod(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String engagementMethodTab()
    {
        return "//div[@id='control_4A471537-8229-4E54-A86C-DCEB99BA24D0']//li";
    }

    public static String engagementPurposeTab()
    {
        return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']//li";
    }

    public static String anyEngagementPurpose(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }
    
     public static String expandInPerson()
    {
        return "(//a[contains(text(),'In-person engagements')]//..//i)[1]";
    }

      public static String engagementDescription()
    {
        return "(//div[@id='control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F']//textarea)[1]";
    }

     public static String responsiblePersonTab()
    {
        return "//div[@id='control_213251A2-010A-4BBF-A65A-A1FC8C6F7033']//li";
    } 
    
     public static String anyResponsiblePerson(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }
     
      public static String Location(String data)
    {
        return "(//div[@id='divForms']/div[13]//a[contains(text(),'" + data + "')])[1]";
    }
    
     public static String audienceType()
    {
        return "//div[@id='control_74C6DEB4-AE07-4476-AF24-753B7187F94A']//li";
    }
     
     public static String contactInquiryOrTopic()
    {
        return "//div[@id='control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019']//li";
    }
     
      public static String natureOfContactInquiryTextbox()
    {
        return "(//div[@id='control_17640BF1-096D-4917-93DC-398CE4D07B1C']//input)[1]";
    }
     
     public static String locationDropdown()
    {
        return "//div[@id='control_F703A144-D0B6-4D4D-B5E2-D4E186427A43']//li";
    }  
     
     public static String engagementOutcome()
    {
        return "(//div[@id='control_180C969D-A1C8-46FF-9F11-EA0457D2F762']//textarea)[1]";
    }
     
     public static String Engagement_save()
    {
        return "//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }
     
     public static String getEngamentRecord()
    {
        return "//div[@class='recnum']//div[@class='record']";
    }

     public static String checklistSelect(String text)
    {
      return "(//a[text()='" + text + "']/i[1])";
    }
     
      public static String EngamentVGRefresh()
    {
        return "//div[@id='control_89186F28-56D9-423C-9FA4-8D688243B982']//a[@class='k-pager-refresh k-link']";
    }
     
     
     //FR18 MS
     public static String engagementPlanTab()
    {
        return "//li[@id='tab_DDBBBB82-6CF9-48FC-9B94-74802828F534']";
    }

    public static String engagementPlanRecordToBeOpened() {
        return "((//div[@id='control_3E81F0BD-F14C-4379-A2EC-610D9F4DE5A9']//div//table)[3]//tr)[1]";

    }
     
    //FR18 AS
    public static String addEngagementPlanButton() {
        return "//div[@id='control_7BFEF019-BD44-41B3-A571-0BC79A458DBF']";
    }
    
    public static String engagementPlanProcess_flow() {
        return "//div[@id='btnProcessFlow_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']";
    }
    public static String engagementPlanTitle() {
        return "(//div[@id='control_185410E8-D077-4DE6-8958-5772CA36E091']//input)[1]";
    }
    public static String engagementPlanStartDate() {
        return "//div[@id='control_90276DFA-A2DD-4A38-8D96-E84491597886']//input[@type='text']";
    }
    public static String planBusinessUnitTab() {
        return "//div[@id='control_8BE367EF-E449-4165-BC05-74385ECBF771']";
    }
    public static String businessUnit_SelectAllBtn(String data) {
        return "//a[text()='" + data + "']";
    }
    public static String EP_projectLink() {
        return "//div[@id='control_DE6A7B48-B355-48A7-AA3E-8475171708AF']/div[1]";
    }
    public static String EP_projectTab() {
        return "//div[@id='control_301410A3-9118-4FCA-93B7-4C2A90320266']";
    }
    public static String frequency() {
        return "//div[@id='control_0189D12C-09E0-4D04-964C-34044E11A982']";
    }
    public static String EP_engagementPurposeTab() {
        return "//div[@id='control_36951962-3063-4DA2-9846-ED7137AFC783']";
    }
    public static String purposeOfEngagement(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String methodOfEngagement() {
        return "//div[@id='control_5D729CA2-07EF-4E0A-B491-1E223A263474']";
    }
    public static String methodOfEngagementSelect(String data) {
        return "//ul[@class='jstree-children']//a[text()='Project Update']";
    }
    public static String personResponsibleTab() {
        return "//div[@id='control_C2B7C6FA-10FC-4593-BD27-6869D1790758']";
    }
    public static String EP_SaveBtn() {
        return "//div[@id='btnSave_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']//div[text()='Save']";
    }        
    
    public static String engagementPlanEndDate() {
        return "//div[@id='control_56848732-729E-4C20-A0FC-1034ACF3D6F4']//input[@type='text']";
    }
    
    public static String businessUnit_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String businessUnit_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    
    public static String projectSelct(String data) {
        return "//a[text()='" + data + "']";
    }
    
     public static String engagementMethodArrow() {
        return "//a[text()='In-person engagements']/../i";
    }
    public static String responsiblePersonSelect(String data) {
        return "//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'" + data + "')]";
    }
    
    public static String SaveToContinue_SaveBtn() {
        return "//div[@id='control_B1893417-CAAD-4DC3-BD27-2EC339FBAE64']//div[text()='Save to continue']";
    }
    
    static String engagementPlanTitle;
    
    public static void setEngagementPlanTitle(String value) {
        engagementPlanTitle = value;
    }
    public static String getEngagementPlanTitle() {
        return engagementPlanTitle;
    }
    
     public static String EngamentPlanVGRefresh()
    {
        return "//div[@id='control_3E81F0BD-F14C-4379-A2EC-610D9F4DE5A9']//a[@class='k-pager-refresh k-link']";
    }
    
     
     //FR19 MS 
     public static String grievancesTab()
    {
        return "//li[@id='tab_87371649-B45E-4E30-8E99-0CE2440653A4']";
    }

    public static String grievancesRecordToBeOpened() {
        return "((//div[@id='control_310658C6-8D55-4419-A635-89E685F26BC7']//div//table)[3]//tr)[1]";

    }
     
    //FR20 MS
    public static String relatedIncidentsTab()
    {
        return "//li[@id='tab_6335906D-BAFA-4BD8-88B8-9DE784776205']";
    }

    public static String incidentsRecordToBeOpened() {
        return "((//div[@id='control_AE8EC219-4756-40BA-8773-68B10FE425BA']//div//table)[3]//tr)[1]";

    }
    
    //FR21 MS
    public static String initiativesTab()
    {
        return "//li[@id='tab_5B060162-47EF-4C59-930A-A09673886172']";
    }

    public static String initiativesRecordToBeOpened() {
        return "((//div[@id='control_BEEA2C5C-D12B-4585-BB9E-8A220737D2C3']//div//table)[3]//tr)[1]";

    }
    
    //FR21 AS
    public static String addInitiativeButton()
    {
        return "//div[@id='control_E181E293-8467-44CC-91D6-5B2D09CBD666']";
    }
    
     public static String initiativesProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']";
    }

     public static String processFlowStatus(String phase)
    {
    return "(//div[text()='"+phase+"'])[2]/parent::div";
    }
     
     public static String processFlowStatusChild(String phase)
    {
    return "(//div[text()='"+phase+"'])[3]/parent::div";
    }

    public static String businessUnitDropdown()
    {
        return "//div[@id='control_1B017CB2-B482-4AE4-AC1E-3E589A354761']//li";
    }  
      
    public static String selectAllDropdown()
    {
        return "//div[@id='control_1B017CB2-B482-4AE4-AC1E-3E589A354761']//b[@original-title='Select all']";
    }  
    
    public static String projectTitle()
    {
        return "(//div[@id='control_5CD5C423-3559-4155-A152-86D25E294254']//input)[1]";
    }
    
    public static String typeOfInitiativeDropdown()
    {
        return "//div[@id='control_2C68539F-B3A7-4844-A919-CCA67BB70A53']//li";
    }  
    
    public static String typeOfInitiativeDropdownValue(String option)
    {
        return "(//div[contains(@class,'transition visible')]//li[@title='"+option+"']//i[@class='jstree-icon jstree-ocl'])[1]";
    }  
    
    public static String typeOfInitiativeDropdownChildValue(String option)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    } 
       
    public static String commencementDateXpath()
    {
        return "//div[@id='control_A02E4959-B4FD-4733-BFCD-8153C6F7CD70']//input";
    }  
    
    public static String deliveryDateXpath()
    {
        return "//div[@id='control_FA763176-78D8-437B-8E60-F14EC6FD89D5']//input";
    }  
    
    public static String approvedBudget()
    {
        return "(//div[@id='control_5E293AF0-02B6-4085-81A8-839A648886CD']//input)[1]";
    }
    
    public static String initaivesLocationDropdown()
    {
        return "//div[@id='control_3A232C9E-B084-4A78-BED5-9FC568351142']//li";
    }
    
    
    public static String expandChevron(String index)
    {
        return "(//div[contains(@class,'transition visible')]//li[@title='South Africa']//i[@class='jstree-icon jstree-ocl'])["+index+"]";
    } 
    
    public static String singleSelect(String value)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='"+value+"']";
    }
    
    public static String responsiblePersonDropdown()
    {
        return "//div[@id='control_C1F24402-51E4-4F49-9520-00D62284121D']//li";
    } 
     
    public static String responsiblePersonDropdownValue(String value)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='"+value+"']";
    }  
      
    public static String sectorDropdown()
    {
        return "//div[@id='control_6EF2C216-A531-46DB-806C-D5A5365021C9']//li";
    }
     
    public static String description()
    {
        return "//div[@id='control_0889A1EE-89BA-4E41-B097-67F10CBD9C92']//textarea";
    } 
    
    public static String saveButton()
    {
        return "//div[@id='btnSave_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']";
    } 
     
     public static String socialInitiativesRecordNumber_xpath() {
        return "(//div[@id='form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']//div[contains(text(),'- Record #')])[1]";
    }
     
    
}

