/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholer_Entity_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class Stakeholder_Entity_FR22toFR26_PageObjects extends BaseClass
{

    //FR22 MS
    public static String rightArrow()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }
    
     public static String commitmentsTab()
    {
        return "//li[@id='tab_5FD00F1C-95C2-48FD-91A0-896E6C72A5A4']";
    }

    public static String commitmentsRecordToBeOpened() {
        return "((//div[@id='control_91B6644B-A674-499F-B3D6-A2B39278BEB2']//div//table)[3]//tr)[1]";

    }
    
    
    //FR22 AS
     public static String addCommitmentsButton()
    {
        return "//div[@id='control_DDF2EE65-8CF7-4105-A624-EF4696F4A796']";
    }

     public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }
     
     public static String commitmentsProcessflow()
    {
        return "//div[@id='btnProcessFlow_form_8541B36E-740A-4367-94DC-BF0661305F0E']";
    } 
    
    public static String processFlowStatus(String phase)
    {
    return "(//div[text()='"+phase+"'])[2]/parent::div";
    }
    
    public static String processFlowStatusChild(String phase)
    {
    return "(//div[text()='"+phase+"'])[3]/parent::div";
    }
    
    public static String businessUnitDropdown()
    {
        return "//div[@id='control_1B017CB2-B482-4AE4-AC1E-3E589A354761']//li";
    }  
      
    public static String expandChevron(String text){
        return "//a[text()='"+text+"']/../i";
    }
    
    public static String businessUnitValue(String text)
    {
        return "(//a[text()='" + text + "']/i[1])";
    }
    
    public static String businessUnitDpdn()
    {
        return "//div[@id='control_1B017CB2-B482-4AE4-AC1E-3E589A354761']//ul";
    } 
    
    public static String projectTilte()
    {
        return "(//div[@id='control_5CD5C423-3559-4155-A152-86D25E294254']//input)[1]";
    }
    
    public static String commencementDateXpath()
    {
         return "//div[@id='control_A02E4959-B4FD-4733-BFCD-8153C6F7CD70']//input";
    }
    
    public static String deliveryDateXpath()
    {
         return "//div[@id='control_FA763176-78D8-437B-8E60-F14EC6FD89D5']//input";
    }
    
    public static String approvedBudget()
    {
        return "(//div[@id='control_5E293AF0-02B6-4085-81A8-839A648886CD']//input)[1]";
    }
    
    public static String singleSelect(String option)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }
     
    public static String singleSelectIndex(String option,String index)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + option + "'])["+ index +"]";
    }
    
    public static String locationDropdown()
    {
        return "//div[@id='control_3A232C9E-B084-4A78-BED5-9FC568351142']//li";
    } 
    
    public static String responsiblePersonDropdown()
    {
        return "//div[@id='control_C1F24402-51E4-4F49-9520-00D62284121D']//li";
    } 
    
    public static String sectorDropdown()
    {
        return "//div[@id='control_6EF2C216-A531-46DB-806C-D5A5365021C9']//li";
    } 
    
    public static String description()
    {
        return "//div[@id='control_0889A1EE-89BA-4E41-B097-67F10CBD9C92']//textarea";
    }  
    
    public static String linkToMeetingDropdown()
    {
        return "//div[@id='control_E781027E-993C-4140-99E3-9A7B0EBF31C1']//li";
    } 
    
    public static String saveButton()
    {
        return "//div[@id='btnSave_form_8541B36E-740A-4367-94DC-BF0661305F0E']";
    } 
    
    public static String commitmentsRecordNumber_xpath() {
        return "(//div[@id='form_8541B36E-740A-4367-94DC-BF0661305F0E']//div[contains(text(),'- Record #')])[1]";
    }
    
    //FR23 MS
    public static String vulnerabilityTab()
    {
        return "//li[@id='tab_11B11DCF-7A24-4B45-B891-74F8E1DA517B']";
    }

    public static String vulnerableMembersRecordToBeOpened() {
        return "((//div[@id='control_D344FC37-35E2-408B-8AA1-3B35E6D39C95']//div//table)[3]//tr)[1]";

    }
    
    public static String vulnerabilityDescription()
    {
        return "//div[@id='control_FC0E3B54-E5AE-4674-A03A-1D30871FF755']//textarea";
    }  
    
    public static String socialStatusPanel()
    {
        return "//div[@id='control_7E1D1634-E831-421B-882E-6C9146CA8CA5']";
    } 
    
    //FR24 MS
     public static String actions_tab(){
        return "//div[text()='Actions']";
    }
    
    public static String actions_add(){
        return "//div[@id='control_6AECC604-74CD-4CA2-8014-252301E5A6E8']//div[text()='Add']";
    }
    
    public static String action_description() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }
    
    public static String departmentResponsibleDropDown() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }
     
    public static String departmentResponsibleDropDownValue(String value) {
        return "(.//a[text()='"+value+"'])[2]";
    }
    
    public static String actionsresponsiblePersonTab() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }
    
    public static String actionsanyResponsiblePerson(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])";
    }
      
    public static String actionDueDate() 
    {
        return ".//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
     
    public static String actions_Save(){
        return "//div[@id='btnSave_form_F72A20BD-FF84-43A1-8729-31F4F84F23C5']";
    }
    
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    
    //FR26 MS
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
   
    public static String navigate_Stakeholders(){
        return "//label[text()='Stakeholder Management']";
    }
    
    public static String navigate_StakeholderEntity(){
        return "//label[text()='Stakeholder Entity']";
    }
    
    public static String ri_esearch(){
        return "//div[@id='btnActApplyFilter']";
    }
    public static String report(){
        return "//div[@id='btnReports']";
    }
    
    public static String view_reports(){
        return "//span[@title='View report ']";
    }

    public static String full_Reports(){
        return "//span[@title='Full report ']";
    }
    
    public static String popup_conf(){
        return "//div[@title='CONTINUE']";
    }
    
    public static String continueButton()
    {
        return "//div[@title='CONTINUE']";
    }
    
    public static String view_wait(){
        return "//div[text()='Entity report']";
    }
    
    public static String full_wait(){
        return "//div[text()='Entity status']";
    }
    
    
}

