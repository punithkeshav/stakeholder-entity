Feature: SugarCRM Feature

  Background: 
    Given I login with my username 'sw_khumalo' and password 'Sibusiso@2015' of SugarCRM
     Then I should see my home page '<firstname>' '<lastname>'
  
  @CreateContact
  Scenario Outline: Create Contacts
    Given I navigate to the Contacts page
     When I create a contact with first name <firstname> and last name <lastname> and email '<email>'
     Then I should see the contact with first name <firstname> <lastname> is created
     When I logout of SugarCRM 
     Then I should see login page
  
    Examples: 
      | firstname | lastname | email          | 
      | Sibusiso  | Zwane    | sz@gmail.com   | 
      | Relay     | Langa    | rl@jhb.dvt.com | 
      | Thando    | Masondo  | tm@yahoo.com   | 
  
  @DeleteContact
  Scenario Outline: Delete Contacts
    Given I navigate to the Contacts page
     When I delete a contact with first name '<firstname>'
     Then I should see the contact with first name '<firstname>' '<lastname>' is deleted  
     When I logout of SugarCRM 
     Then I should see login page
    Examples: 
      | firstname | lastname | email          | 
      | Sibusiso  | Zwane    | sz@gmail.com   | 
      | Relay     | Langa    | rl@jhb.dvt.com | 
      | Thando    | Masondo  | tm@yahoo.com   | 
  
  @CreateAndDeleteContacts
  Scenario Outline: Create And Delete Contacts
    Given I navigate to the Contacts page
     When I create a contact with first name '<firstname>', last name '<lastname>', and email '<email>'
     Then I should see the contact with first name '<firstname>' '<lastname>' is created
     When I delete a contact with first name '<firstname>'
     Then I should see the contact with first name '<firstname>' '<lastname>' is deleted
     When I logout of SugarCRM 
     Then I should see login page
    Examples: 
      | firstname | lastname | email          | 
      | Sibusiso  | Zwane    | sz@gmail.com   | 
      | Relay     | Langa    | rl@jhb.dvt.com | 
      | Thando    | Masondo  | tm@yahoo.com   | 
  
  
